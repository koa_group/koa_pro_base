const {Model} = require('sequelize');
const Sequelize = require('sequelize')
const db = require('../lib/db')

const wxUser = require('./wxUser')

class Events extends Model{}

Events.init({
  eId: {
    type: Sequelize.INTEGER,
    primaryKey: true,
    autoIncrement: true,
  },
  eName: {
    type: Sequelize.STRING,
    allowNull: false,
  },
  eContent: {
    type: Sequelize.STRING,
    allowNull: false,
  },
  eStarttime: Sequelize.DATE,
  eEndtime: Sequelize.DATE,
},{
  sequelize: db,
  timestamps: true,
  tableName: 'events',
})

Events.userId = Events.belongsTo(wxUser)

Events.sync()

module.exports = Events;