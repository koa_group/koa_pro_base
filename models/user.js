const { Model } = require('sequelize')
const Sequelize = require('sequelize')
const db = require('../lib/db')

class User extends Model {}
User.init({
  id: {
    type: Sequelize.INTEGER,
    primaryKey: true,
    autoIncrement: true,
  },
  name: Sequelize.STRING
}, {
  sequelize: db,
  timestamps: true,
  tableName: 'user1',
})

// 若表不存在则创建
User.sync()

module.exports = User