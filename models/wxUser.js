const { Model } = require('sequelize')
const Sequelize = require('sequelize')
const db = require('../lib/db')
const jwt = require('jsonwebtoken')
const config = require('../config')

class WxUser extends Model {}
WxUser.init({
  token: {
    type: Sequelize.VIRTUAL,
    get() {
      return jwt.sign({ openid: this.openid }, config.secret)
    }
  },
  openid: {
    type: Sequelize.STRING,
    allowNull: false,
  },
  avatarUrl: {
    type: Sequelize.STRING,
    allowNull: false,
  },
  nickName: {
    type: Sequelize.STRING,
    allowNull: false,
  },
  city: {
    type: Sequelize.STRING,
    allowNull: false,
  },
  country: {
    type: Sequelize.STRING,
    allowNull: false,
  },
  province: {
    type: Sequelize.STRING,
    allowNull: false,
  },
  gender: {
    type: Sequelize.STRING,
    allowNull: false,
  },
  language: {
    type: Sequelize.STRING,
    allowNull: false,
  },
}, {
  sequelize: db,
  timestamps: true,
  tableName: 'wx_user',
  charset: 'utf8mb4',
})

// 若表不存在则创建
WxUser.sync()

module.exports = WxUser