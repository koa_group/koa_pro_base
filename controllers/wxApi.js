
const config = require('../config');
const koa2Req = require('koa2-request');
const wxUser = require('../models/wxUser');

const wxApi = {}

wxApi.getCode = async (ctx, next) => {
  const {code} = ctx.request.body
  // 组合url
  let url = 'https://api.weixin.qq.com/sns/jscode2session?appid='+config.wx.appId+'&secret='+config.wx.appKey+'&js_code='+code+'&grant_type=authorization_code'
  // 向微信服务器发送请求
  let res = await koa2Req(url);
  // 获取session_key和openid
  ctx.result = JSON.parse(res.body);
  return next();
}

wxApi.login = async (ctx, next) => {
  const {openid} = ctx.request.body
  const res = await wxUser.findOrCreate({
    where: { openid },
    defaults: ctx.request.body
  });
  ctx.result = res[0];
  return next();
}

module.exports = wxApi;