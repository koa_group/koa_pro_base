
const Events = require('../models/events')
const WxUser = require('../models/wxUser')

const events = {}

events.create = async (ctx, next) => {
  ctx.result = await Events.create(ctx.request.body)
  return next()
}

events.search = async (ctx, next) => {
  ctx.result = await Events.findAll({include: WxUser})
  return next()
}

module.exports = events;