const Router = require('koa-router')
const controllers = require('../controllers')
const jwtMiddleware = require('../middlewares/jwt')

const router = new Router()
router.prefix('/api/v1')
router.use(jwtMiddleware)


router.get('/query', controllers.query.findSomething)

router.get('/getUser', controllers.crud.search)
router.post('/addUser', controllers.crud.add)

router.post('/addEvent', controllers.events.create)
router.get('/getEvent', controllers.events.search)

module.exports = router