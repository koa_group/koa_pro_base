const Router = require('koa-router')
const controllers = require('../controllers')

const router = new Router()
router.prefix('/api/v1')

router.post('/login', controllers.login.login)
// 微信相关
router.post('/wxGetCode', controllers.wxApi.getCode)
router.post('/wxLogin', controllers.wxApi.login)
module.exports = router 